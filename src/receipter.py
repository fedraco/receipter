from PIL import Image
import pytesseract
from pathlib import Path
import argparse
import yaml
from datetime import date
from typing import Optional, Tuple

import re

CFG_EXTS = ("yml", "yaml")
DEFAULT_CFG_FNAME_WO_EXT = "receipter"
DEFAULT_CFG_DPATH = Path.home() / ".config"
DATE_REGEX_STR = r"(?:\d[ \t]?){2}/[ \t]?(?:\d[ \t]?){2}/(?:[ \t]?\d){4}"

def find_cfg_file(cfg_dpath: Path, fname_wo_ext: str = DEFAULT_CFG_FNAME_WO_EXT) -> Optional[Path]:
    cfg_path = None
    for ext in CFG_EXTS:
        cfg_path = cfg_dpath / (fname_wo_ext + "." + ext)
        if cfg_path.exists():
            break
    else:
        raise FileNotFoundError("could not find any configuration file")
    return cfg_path


def load_cfg(cfg_path: Path):
    with open(cfg_path, "r") as fd:
        return yaml.safe_load(fd)


def date_from_image(image_path) -> Tuple[str, str, str]:
    image_str = pytesseract.image_to_string(Image.open(image_path), config="--psm 6", lang='fra')
    pattern = re.compile(DATE_REGEX_STR)
    matches = pattern.findall(image_str)
    if not matches:
        raise ValueError("Image does not contain a legible date")
    preprocessed = matches[-1]
    processed = "".join(preprocessed.split())
    day, month, year = processed.split("/")
    return year, month, day


def process_inbox(inbox_dpath: Path, processed_dpath: Path, image_name_prefix: str = ""):
    processed_dpath.mkdir(exist_ok=True)
    for image_path in inbox_dpath.glob("*.[jp][pn]g"):
        year, month, day = date_from_image(image_path)
        ext = image_path.suffix
        new_image_basename = f"{image_name_prefix}{year}{month}{day}"
        new_image_path = processed_dpath / (new_image_basename + ext)
        duplicate_counter = 0
        while new_image_path.exists():
            if duplicate_counter > 99:
                raise Exception("image cannot be moved to processed directory")
            duplicate_counter += 1
            new_image_path = processed_dpath / f"{new_image_basename}_{duplicate_counter}{ext}"
        image_path.rename(new_image_path)


def main(args):
    if args.cfg_path is not None:
        cfg = load_cfg(args.cfg_path)
    else:
        cfg = load_cfg(find_cfg_file(args.cfg_dir))
    inbox_dpath = Path(cfg['inbox_dir']).expanduser()
    processed_dpath = Path(cfg['processed_dir']).expanduser()
    image_name_prefix = cfg['image_name_prefix'] if 'image_name_prefix' in cfg else ""
    if not args.quiet:
        print("Processing images...")
    process_inbox(inbox_dpath, processed_dpath, image_name_prefix)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Automatically rename receipts based on their date")
    parser.add_argument("--cfg-dir", default=DEFAULT_CFG_DPATH)
    parser.add_argument("--cfg-path")
    parser.add_argument("--quiet", action="store_true")
    args = parser.parse_args()
    main(args)
    
